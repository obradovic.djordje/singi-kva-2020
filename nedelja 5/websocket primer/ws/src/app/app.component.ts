import { Component } from '@angular/core';

// import { HttpClient} from '@angular/common/http';
import { webSocket } from 'rxjs/webSocket' // for RxJS 6, for v5 use Observable.webSocket


export class AppMessage{
  public sender:String;
  public body:String;
  public choice:boolean;
  constructor(){}

  equals(a:AppMessage):boolean{
    if(this.sender ==a.sender)
      return true;
    else
      return false;
  }

}


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'ws';

  private wsclient;
  private username = "";
  private msg = "";
  private listaYes = new Set<AppMessage>();  
  private listaNo = new Set<AppMessage>();


  constructor() {
    this.wsclient = webSocket('ws://localhost:8080/singidunum');
    this.wsclient.subscribe(
       (msg)=>{
         if(msg == 'clearAll'){
              this.listaNo = new Set<AppMessage>();
              this.listaYes = new Set<AppMessage>();
          }else{
              let obj: AppMessage = JSON.parse(msg);
              console.log(obj.choice);
              if(obj.choice == true){
                this.listaYes.add(obj);
              }else{
                this.listaNo.add(obj);
              }
          }
        },
       (err) => console.log(err),
       () => console.log('complete')
     );
  }

  send(choice:boolean){
      let msg:AppMessage = new AppMessage();
      msg.body = this.msg;
      msg.sender = this.username;
      msg.choice = choice;
      this.wsclient.next(JSON.stringify(msg));
      console.log('tu sam');
  }

  clear(){
    this.listaNo = new Set<AppMessage>();
    this.listaYes = new Set<AppMessage>();
    this.wsclient.next('clearAll');
  }

}
