from tornado import websocket, web, ioloop

# sudo pip install tornado

cl = []


class SocketHandler(websocket.WebSocketHandler):
    def check_origin(self, origin):
        return True

    def open(self):
        print('new client')
        if self not in cl:
            cl.append(self)

    def on_message(self, message):
        print(message)
        for cc in cl:
            cc.write_message(message)

    def on_close(self):
        if self in cl:
            print('client left')
            cl.remove(self)


app = web.Application([
    (r'/singidunum', SocketHandler)
])

if __name__ == '__main__':
    lib_dir = ""
    app.listen(8080)
    # app.listen(9001, ssl_options={
    #     "certfile": "/etc/nginx/ssl/brazil.crt",
    #     "keyfile": "/etc/nginx/ssl/brazil.key"
    # })
    # "certfile": "/home/djordje/data/git/brazil/ssl/keys/chained.pem",
    # "keyfile": "/home/djordje/data/git/brazil/ssl/keys/brazil.nsi.rs.key",
    ioloop.IOLoop.instance().start()
