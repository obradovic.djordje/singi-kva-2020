from websocket import create_connection

# pip install websocket-client

import json


def send_ws_message(ws_message):
    message = json.dumps(ws_message)
    ws = create_connection("ws://localhost:8080/websocket")
    ws.send(message)
    ws.close()

def send_ws_spring_message(ws_message):
    message = json.dumps(ws_message)
    ws = create_connection("ws://localhost:8080/ws")
    print('tu sam')
    ws.send(message)
    ws.close()



send_ws_message(json.dumps({'ime':"Petar", "prezime":"Petrovic"}))

#send_ws_spring_message(json.dumps({'name':"poruka"}))
