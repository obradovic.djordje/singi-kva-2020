import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';

import * as THREE from 'three';


@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit, AfterViewInit {
 
  constructor() { }

  ngOnInit(): void {
    var scene = new THREE.Scene();
    var camera = new THREE.PerspectiveCamera( 75, window.innerWidth/window.innerHeight, 0.1, 1000 );
    
    var renderer = new THREE.WebGLRenderer();
    renderer.setSize( window.innerWidth, window.innerHeight );
    document.body.appendChild( renderer.domElement );
    
    var geometry = new THREE.BoxGeometry( 1, 1, 1 );
    var material = new THREE.MeshBasicMaterial( { color: 0x00ff00 } );
    var cube = new THREE.Mesh( geometry, material );
    scene.add( cube );

    var geometry2 = new THREE.BoxGeometry( 0.5, 1, 0.5 );
    var material2 = new THREE.MeshBasicMaterial( { color: 0xff0000 } );
    var cube2 = new THREE.Mesh( geometry2, material2 );
    cube2.position.x = 2;
    scene.add( cube2 );


    camera.position.z = 5;
    
    var animate = function () {
      requestAnimationFrame( animate );
    
      cube.rotation.x += 0.01;
      cube.rotation.y += 0.01;
    
      renderer.render( scene, camera );
    };
    
    animate();
  }

  ngAfterViewInit() {

  }

}
