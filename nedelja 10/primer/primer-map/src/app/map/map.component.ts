import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';

import Map from 'ol/Map';
import View from 'ol/View';
import * as proj from 'ol/proj';
import Point from 'ol/geom/Point'
import Feature from 'ol/Feature';
import {Tile as TileLayer, Vector as VectorLayer} from 'ol/layer';
import {OSM, Vector as VectorSource} from 'ol/source';
import {CdkDragDrop, moveItemInArray, transferArrayItem, CdkDrag} from '@angular/cdk/drag-drop';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit, AfterViewInit {
  @ViewChild("mapView")
  mapView;

  @ViewChild("marker")
  marker;

  private map;
  private raster;
  private source;
  private vector;
  itemList = [
    {
      naslov: "Singidunum - Novi Sad",
      geom: [19.84442, 45.2531]
    },
    {
      naslov: "Singidunum - Beograd",
      geom: [20.4790755, 44.7822791]
    }
  ];

  mapList = [];

  constructor() { }

  ngOnInit(): void {
    this.raster = new TileLayer({
      source: new OSM()
    });

    this.source = new VectorSource({wrapX: false});

    this.vector = new VectorLayer({
      source: this.source
    });
  }

  ngAfterViewInit() {
    this.map = new Map({
      layers: [this.raster, this.vector],
      target: this.mapView.nativeElement,
      view: new View({
        center: proj.fromLonLat([19.84442, 45.2531]),
        zoom: 8
      })
    });
  }

  drop(event: CdkDragDrop<any>) {
    let data = event.previousContainer.data[event.previousIndex];
    transferArrayItem(event.previousContainer.data, event.container.data, event.previousIndex, event.currentIndex);
    this.source.addFeature(new Feature({
      geometry: new Point(proj.fromLonLat(data["geom"]))
    }));
  }
}
