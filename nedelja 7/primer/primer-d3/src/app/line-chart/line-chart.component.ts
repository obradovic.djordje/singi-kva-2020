import { Component, OnInit, ElementRef, ViewChild, Input, OnChanges, SimpleChanges } from '@angular/core';
import * as d3 from 'd3';

@Component({
  selector: 'app-line-chart',
  templateUrl: './line-chart.component.html',
  styleUrls: ['./line-chart.component.css']
})
export class LineChartComponent implements OnInit, OnChanges {

  @Input("data")
  data = []

  @Input()
  width: number = 500;
  @Input()
  height: number = 500;
  @Input()
  leftMargin: number = 25;
  @Input()
  rightMargin: number = 25;
  @Input()
  topMargin: number = 25;
  @Input()
  bottomMargin: number = 25;

  private svg;
  private lineGenerator;
  private xScale;
  private yScale;
  private chart;
  private xAxis;
  private yAxis;
  private axes;


  constructor(private element: ElementRef) { }

  ngOnInit(): void {
    this.svg = d3.select(this.element.nativeElement).append("svg").attr('height', this.height).attr("width", this.width);
    
    this.lineGenerator = d3.line().x(d => this.xScale(d["x"])).y(d => this.yScale(d["y"])).curve(d3.curveLinear);

    this.axes = this.svg.append("g");
    this.chart = this.svg.append("g").attr("transform", `translate(${this.leftMargin}, ${0})`).append("path");
    
    this.updateAxes();
    this.updateLine();
  }

  private updateLine() {
    this.chart.datum(this.data).attr("class", "line").attr("d", <any>this.lineGenerator(<any>this.data));
  }

  private updateAxes() {
    this.xScale = d3.scaleLinear().domain(d3.extent(this.data, d => d.x)).range([0, this.width - this.leftMargin]);
    this.yScale = d3.scaleLinear().domain(d3.extent(this.data, d => d.y)).nice().range([this.height - this.bottomMargin, this.topMargin]);
    this.xAxis = g => g.attr("transform", `translate(${this.leftMargin},${this.height - this.bottomMargin})`)
      .call(d3.axisBottom(this.xScale));


    this.yAxis = g => g.attr("transform", `translate(${this.leftMargin},0)`)
      .call(d3.axisLeft(this.yScale));

    this.axes.selectAll("g").remove();
    this.axes.append("g").attr("class", "axis-x").call(this.yAxis);
    this.axes.append("g").attr("class", "axis-y").call(this.xAxis);
  }

  public ngOnChanges(changes: SimpleChanges) {
    if ((changes.data.currentValue != changes.data.previousValue) && (!changes.data.isFirstChange())) {
      this.updateAxes();
      this.updateLine();
    }
  }
}
