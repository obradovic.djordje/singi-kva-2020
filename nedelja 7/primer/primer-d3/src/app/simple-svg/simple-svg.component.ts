import { Component, OnInit, ElementRef, } from '@angular/core';
import * as d3 from 'd3';

@Component({
  selector: 'app-simple-svg',
  templateUrl: './simple-svg.component.html',
  styleUrls: ['./simple-svg.component.css']
})
export class SimpleSvgComponent implements OnInit {

  private svg;
  private width = 500;
  private height = 400;


  constructor(private element: ElementRef) { }

  ngOnInit(): void {
    this.svg = d3.select(this.element.nativeElement).append("svg").attr('height', this.height).attr("width", this.width);
    for (let y = 10; y < 100; y += 10) {
      this.svg.append('line')
        .style("stroke", "green")
        .attr("x1", 10).attr("y1", y)
        .attr("x2", 100).attr("y2", y);
    }

    // this.svg.append('line')
    //   .style("stroke", "red")
    //   .attr("x1", 100).attr("y1", 10)
    //   .attr("x2", 100).attr("y2", 100).attr("transform", `translate(20,0)`);
  }
}
