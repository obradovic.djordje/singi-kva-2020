import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UmlDiagramComponent } from './uml-diagram.component';

describe('UmlDiagramComponent', () => {
  let component: UmlDiagramComponent;
  let fixture: ComponentFixture<UmlDiagramComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UmlDiagramComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UmlDiagramComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
