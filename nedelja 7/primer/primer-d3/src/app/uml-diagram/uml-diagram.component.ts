import { Component, OnInit, Input, ElementRef, OnChanges, SimpleChanges } from '@angular/core';
import * as d3 from 'd3';

@Component({
  selector: 'app-uml-diagram',
  templateUrl: './uml-diagram.component.html',
  styleUrls: ['./uml-diagram.component.css']
})
export class UmlDiagramComponent implements OnInit, OnChanges {

  @Input("data")
  data = []

  @Input()
  width: number = 500;
  @Input()
  height: number = 500;
  @Input()
  leftMargin: number = 25;
  @Input()
  rightMargin: number = 25;
  @Input()
  topMargin: number = 25;
  @Input()
  bottomMargin: number = 25;
  @Input()
  minRectWidth: number = 100;
  @Input()
  minRecthHeight: number = 150;
  @Input()
  rectPadding: number = 5;

  private svg;
  private classes;

  constructor(private element: ElementRef) { }

  ngOnInit(): void {
    this.svg = d3.select(this.element.nativeElement).append("svg");
    this.svg.attr("height", this.height);
    this.svg.attr("width", this.width);
    this.classes = this.svg.append("g");
    this.classes.attr("transform", `translate(${this.leftMargin}, ${this.topMargin})`)
    this.update();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if ((changes.data.currentValue != changes.data.previousValue) && (!changes.data.isFirstChange())) {
      this.update();
    }
  }

  private createNodes(parent) {
    let classNode = parent.append("g");
    let background = classNode.append("g");
    let foreground = classNode.append("g");
    let title = foreground.append("g");
    let attributes = foreground.append("g");
    let operations = foreground.append("g");

    foreground.attr("transform", `translate(${this.rectPadding}, ${this.rectPadding})`);

    title.append("text")
      .attr("alignment-baseline", "hanging")
      .text(d => d.name)
      .attr("font-weight", "bold")
      .attr("y", "0");

    let titleBBox = title.nodes().map(d => d.getBBox())
    let titleHeight = titleBBox.map(d => d.height);

    attributes.attr("transform", (_, i) => `translate(0,${titleHeight[i] + this.rectPadding})`);
    attributes.selectAll("text").data(d => d.attributes)
      .join(d => this.createAttributes(d));

    let attributesBbox = attributes.nodes().map(d => d.getBBox())
    let attributesHeight = attributesBbox.map(d => d.height);

    operations.attr("transform", (_, i) => `translate(0,${attributesHeight[i] + titleHeight[i] + this.rectPadding * 2})`);
    operations.selectAll("text").data(d => d.operations)
      .join(d => this.createOperations(d));

    let totalBbox = foreground.nodes().map(d => d.getBBox())
    let totalWidth = totalBbox.map(d => Math.max(d.width + this.rectPadding * 2, this.minRectWidth));
    let totalHeight = totalBbox.map(d => Math.max(d.height + this.rectPadding * 2, this.minRecthHeight));

    background.append("rect").attr("class", "diagram-rect")
      .attr("x", "0")
      .attr("y", "0")
      .attr("width", (_, i) => `${totalWidth[i]}`)
      .attr("height", (_, i) => `${totalHeight[i]}`);


    title.select("text").attr("x", (_, i) => `${totalWidth[i] / 2}`).attr("text-anchor", "middle");
    background.append("path").attr("d", (_, i) => `M${0},${titleHeight[i] + this.rectPadding}L${totalWidth[i]},${titleHeight[i] + this.rectPadding}`).attr("stroke", "black");
    background.append("path").attr("d", (_, i) => `M${0},${attributesHeight[i] + titleHeight[i] + this.rectPadding * 2}L${totalWidth[i]},${attributesHeight[i] + titleHeight[i] + this.rectPadding * 2}`).attr("stroke", "black");
    return classNode;
  }

  private createAttributes(parent) {
    return parent.append("text")
      .text(d => `${d.visibility} ${d.name}: ${d.type}`)
      .attr("alignment-baseline", "hanging")
      .attr("x", "0")
      .attr("class", "attribute")
      .attr("y", function (_, i) {
        return i * this.getBBox().height;
      });
  }

  private createOperations(parent) {
    return parent.append("text")
      .text(d => `${d.visibility} ${d.name}(${d.parameteres.map(d => `${d.name}: ${d.type}`).join(", ")}): ${d.returnType}`)
      .attr("alignment-baseline", "hanging")
      .attr("x", "0")
      .attr("class", "operation")
      .attr("y", function (_, i) {
        return i * this.getBBox().height;
      });
  }

  update() {
    this.classes.selectAll("g").remove();
    this.classes.selectAll("g").data(this.data).join(d => this.createNodes(d))
      .attr("transform", (_, i, g) => {
        /*
         * Rasporedjivanje elemenata na osnovu
         * indeksa i sirine svakog prikaza klase.
         */
        let x = 0;
        for (let j = 0; j < i; j++) {
          x += g[j].getBBox().width;
        }
        return `translate(${i * 10 + x}, 0)`;
      });
  }
}
