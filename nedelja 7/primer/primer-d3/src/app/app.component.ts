import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  numericData = [];

  diagramDataPool = [
    [
      {
        name: "Person",
        attributes: [
          {
            name: "name",
            type: "String",
            visibility: "-"
          },
          {
            name: "surname",
            type: "String",
            visibility: "-"
          },
          {
            name: "birthday",
            type: "LocalDateTime",
            visibility: "-"
          }
        ],
        operations: [
          {
            name: "getAge",
            parameteres: [],
            returnType: "int",
            visibility: "+"
          }
        ]
      },
      {
        name: "Account",
        attributes: [
          {
            name: "name",
            type: "String",
            visibility: "-"
          },
          {
            name: "accountNumber",
            type: "String",
            visibility: "-"
          },
          {
            name: "funds",
            type: "Double",
            visibility: "-"
          }
        ],
        operations: [
          {
            name: "transfer",
            parameteres: [
              { name: "destination", type: "Account" },
              { name: "amount", type: "Double" }
            ],
            visibility: "+",
            returnType: "void"
          }
        ]
      },
      {
        name: "Transaction",
        attributes: [
          {
            name: "source",
            type: "Account",
            visibility: "-"
          },
          {
            name: "destination",
            type: "Account",
            visibility: "-"
          },
          {
            name: "amount",
            type: "Double",
            visibility: "-"
          }
        ],
        operations: []
      }
    ],
    [
      {
        name: "Student",
        attributes: [
          {
            name: "name",
            type: "String",
            visibility: "-"
          },
          {
            name: "surname",
            type: "String",
            visibility: "-"
          },
          {
            name: "birthday",
            type: "LocalDateTime",
            visibility: "-"
          }
        ],
        operations: [
          {
            name: "getAge",
            parameteres: [],
            returnType: "int",
            visibility: "+"
          }
        ]
      }
    ]
  ]

  diagramData = this.diagramDataPool[0];

  public ngOnInit() {
    this.newData();
  }

  public newData() {
    let length = Math.trunc(Math.random() * 15 + 10);
    let tmp = [];
    for (let i = 0; i < length; i++) {
      tmp.push({
        x: i * 10,
        y: Math.random() * 200
      })
    }
    this.diagramData = this.diagramDataPool[Math.trunc(Math.random()*(this.diagramDataPool.length))];
    this.numericData = tmp;
  }
}
