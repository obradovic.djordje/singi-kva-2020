import { Component, OnInit } from '@angular/core';
import { Table, Row } from 'my-angular-table';
import { HttpClient } from '@angular/common/http';
import { MainService } from '../shared/service/main.service';
import { Route } from '@angular/compiler/src/core';
import { Router } from '@angular/router';

export declare interface Projekcija {
  id: number,
  filmId: number,
  sala: string,
  pocetak: string
}

@Component({
  selector: 'app-projekcije',
  templateUrl: './projekcije.component.html',
  styleUrls: ['./projekcije.component.css']
})
export class ProjekcijeComponent implements OnInit {
  public table: Table;
  constructor(private httpClient: HttpClient, private mainService: MainService, private router: Router) {}

  findAll = (): void => {
    this.httpClient
        .get(
            "http://localhost:3000/projekcija"
        )
        .subscribe(
            (projekcije: Projekcija[]) => {
            projekcije.forEach((projekcija: Projekcija) => {
                //   console.log(projekcija);
                });

                this.mainService.projekcije = projekcije;
                this.updateTable();
            },
            err => {
                console.log(err);
            }
        );
  }

  onDeleteKarta = (projekcije: Projekcija[]): void => {
    // console.log(projekcije);
    projekcije.forEach((projekcija: Projekcija) => {
      // console.log(projekcija);
      this.httpClient
        .delete(
          "http://localhost:3000/projekcija/"+projekcija.id
        )
        .subscribe(
          result => {
            console.log(result);
            this.findAll();
          },
          err => {
            console.log(err);
          }
        )
    });
  }

  updateTable = (): void => {
    // deleting
    this.table.getChildren().forEach((r: Row) => this.table.removeChild(r));
    
    this.mainService.projekcije.forEach((projekcija: Projekcija) => {
      // console.log(projekcija);

      let timeData = projekcija.pocetak.split(" ");
      let ymd = timeData[0].split("-");
      let hm = timeData[1].split(":");

      let pocetak = new Date(+ymd[0], +ymd[1], +ymd[2], +hm[0], +hm[1]);

      this.table.addChildValue({
        data: projekcija,
        rowItems: [
          {context: projekcija.id},
          {context: projekcija.filmId},
          {context: projekcija.sala},
          {context: pocetak},
          {context: "delete", button: {function: this.onDeleteKarta}, imgSrc: "https://img.icons8.com/dusk/2x/filled-trash.png"},
          {context: "update", button: {function: this.onUpdateProjekcija}, imgSrc: "https://img.icons8.com/nolan/2x/approve-and-update.png"}
        ]
      });
      
    });
  }
  
  onUpdateProjekcija = (projekcije: Projekcija[]): void => {
    this.router.navigate(['projekcije', 'update', projekcije[0].id]);
  }

  ngOnInit(): void {
    this.mainService.attach(this.updateTable);

    this.table = new Table({
      header: {
        headerItems: [
          {context: "id"},
          {context: "id fila"},
          {context: "sala"},
          {context: "pocetak"},
          {context: ""},
          {context: ""}
        ]
      },
      contextMenu: {
        contextMenuItems: [
          {context: "delete", function: this.onDeleteKarta, imgSrc: "https://img.icons8.com/dusk/2x/filled-trash.png"}
        ]
      }
    });

    this.updateTable();

    console.log("ProjekcijeComponent init");
  }

  ngOnDestroy(): void {
    if (this.mainService)
      this.mainService.dettach(this.updateTable);
    console.log("ProjekcijeComponent destroyed");
  }

}
