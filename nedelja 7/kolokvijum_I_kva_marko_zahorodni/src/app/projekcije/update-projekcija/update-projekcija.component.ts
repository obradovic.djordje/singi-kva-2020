import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MainService } from 'src/app/shared/service/main.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Projekcija } from '../projekcije.component';
import { NgForm } from '@angular/forms';
import { Film } from 'src/app/filmovi/filmovi.component';

@Component({
  selector: 'app-update-projekcija',
  templateUrl: './update-projekcija.component.html',
  styleUrls: ['./update-projekcija.component.css']
})
export class UpdateProjekcijaComponent implements OnInit, OnDestroy {
  public filmovi: Film[] = [];
  public projekcija: Projekcija = {
    id: 0,
    filmId: 0,
    pocetak: "",
    sala: ""
  }

  constructor(private httpClient: HttpClient, private mainService: MainService, private aRoute: ActivatedRoute, private router: Router) { }

  convert = (str: string): string => {
    var date = new Date(str),
      mnth = ("0" + (date.getMonth() + 1)).slice(-2),
      day = ("0" + date.getDate()).slice(-2);
    return [date.getFullYear(), mnth, day].join("-");
  }

  onSubmit = (form: NgForm): void => {
    console.log(form);
    if(form.valid) {
      let pocetak = this.convert(form.value.pocetak) + " " + form.value.sati + ":" + form.value.minute;

      this.projekcija.pocetak = pocetak;
      // console.log(this.projekcija);

      // let projekcija = {
      //   filmId: form.value.filmId,
      //   sala: form.value.sala,
      //   pocetak: pocetak
      // }

      this.httpClient
        .put(
          "http://localhost:3000/projekcija/" + this.aRoute.snapshot.params.id,
          this.projekcija
        )
        .subscribe(
          result => {
            this.mainService.findAllProjekcije();
            this.router.navigate(['/projekcije']);
          },
          err => {

          }
        )
    }
  }

  findAllFilm = (): void => {
    this.httpClient
        .get(
            "http://localhost:3000/film"
        )
        .subscribe(
            (filmovi: Film[]) => {
                filmovi.forEach((film: Film) => {
                    // console.log(film);
                });
                this.filmovi = filmovi;
            },
            err => {
                console.log(err);
            }
        );
    }

  ngOnInit(): void {

    this.httpClient
      .get<Projekcija>(
        "http://localhost:3000/projekcija/"+this.aRoute.snapshot.params.id
      )
      .subscribe(
        projekcija => {
          this.projekcija = projekcija;
        },
        err => {

        }
      );

    this.findAllFilm();

    console.log(this.aRoute.snapshot.params);
  }

  ngOnDestroy(): void {
    
  }

}
