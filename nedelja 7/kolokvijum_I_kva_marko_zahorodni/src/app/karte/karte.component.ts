import { Component, OnInit } from '@angular/core';
import { Table, Row } from 'my-angular-table';
import { HttpClient } from '@angular/common/http';
import { MainService } from '../shared/service/main.service';
import { Router } from '@angular/router';

export declare interface Karta {
  id: number,
  projekcijaId: number,
  cena: number
}

@Component({
  selector: 'app-karte',
  templateUrl: './karte.component.html',
  styleUrls: ['./karte.component.css']
})
export class KarteComponent implements OnInit {
  public table: Table;
  constructor(private httpClient: HttpClient, private mainService: MainService, private router: Router) { }

  findAll = (): void => {
    this.httpClient
      .get(
          "http://localhost:3000/karta"
      )
      .subscribe(
          (karte: Karta[]) => {
              karte.forEach((karta: Karta) => {
                  console.log(karta);
              });

              this.mainService.karte = karte;
              this.updateTable();
          },
          err => {
              console.log(err);
          }
      );
  }

  onDeleteKarta = (karte: Karta[]): void => {
    // console.log(karte);
    karte.forEach((karta: Karta) => {
      // console.log(karta);
      this.httpClient
        .delete(
          "http://localhost:3000/karta/"+karta.id
        )
        .subscribe(
          result => {
            console.log(result);
            this.findAll();
          },
          err => {
            console.log(err);
          }
        )
    });
  }

  onUpdateKarta = (karte: Karta[]): void => {
    this.router.navigate(['karte', 'update', karte[0].id]);
  }

  updateTable = (): void => {
    // deleting
    this.table.getChildren().forEach((r: Row) => this.table.removeChild(r));
    
    this.mainService.karte.forEach((karta: Karta) => {
      // console.log(karta);

      this.table.addChildValue({
        data: karta,
        rowItems: [
          {context: karta.id},
          {context: karta.projekcijaId},
          {context: karta.cena},
          {context: "delete", button: {function: this.onDeleteKarta}, imgSrc: "https://img.icons8.com/dusk/2x/filled-trash.png"},
          {context: "update", button: {function: this.onUpdateKarta}, imgSrc: "https://img.icons8.com/nolan/2x/approve-and-update.png"}
        ]
      });
      
    });
  }

  ngOnInit(): void {
    this.mainService.attach(this.updateTable);

    this.table = new Table({
      header: {
        headerItems: [
          {context: "id"},
          {context: "id projekcije"},
          {context: "cena"},
          {context: ""},
          {context: ""}
        ]
      },
      contextMenu: {
        contextMenuItems: [
          {context: "delete", function: this.onDeleteKarta, imgSrc: "https://img.icons8.com/dusk/2x/filled-trash.png"}
        ]
      }
    });

    this.updateTable();

    console.log("KarteComponent init");
  }

  ngOnDestroy(): void {
    if (this.mainService)
      this.mainService.dettach(this.updateTable);
    console.log("KarteComponent destroyed");
  }

}
