import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MainService } from 'src/app/shared/service/main.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Film } from '../filmovi.component';
import { Projekcija } from 'src/app/projekcije/projekcije.component';
import { Table, Row } from 'my-angular-table';

@Component({
  selector: 'app-details-film',
  templateUrl: './details-film.component.html',
  styleUrls: ['./details-film.component.css']
})
export class DetailsFilmComponent implements OnInit {
  public projekcije: Projekcija[] = [];
  public film: Film = {
    id: 0,
    naziv: "",
    ocena: 0,
    reziser: ""
  };

  public table: Table;

  constructor(private httpClient: HttpClient, private mainService: MainService, private aRoute: ActivatedRoute, private router: Router) { }

  findAllProjekcije = (): void => {
    this.httpClient
        .get(
            "http://localhost:3000/projekcija"
        )
        .subscribe(
          (projekcije: Projekcija[]) => {
            let theProjekcije = [];

            projekcije.forEach((projekcija: Projekcija) => {
                //   console.log(projekcija);
              
              if(projekcija.filmId === this.film.id) {
                theProjekcije.push(projekcija);
              }
            });

            this.projekcije = theProjekcije;
            // console.log(this.projekcije);
            this.updateTable();
          },
          err => {
              console.log(err);
          }
      );
  }

  updateTable = (): void => {
    // deleting
    this.table.getChildren().forEach((r: Row) => this.table.removeChild(r));
    
    this.projekcije.forEach((projekcija: Projekcija) => {
      // console.log(projekcija);

      let timeData = projekcija.pocetak.split(" ");
      let ymd = timeData[0].split("-");
      let hm = timeData[1].split(":");

      let pocetak = new Date(+ymd[0], +ymd[1], +ymd[2], +hm[0], +hm[1]);

      this.table.addChildValue({
        data: projekcija,
        rowItems: [
          {context: projekcija.id},
          {context: projekcija.filmId},
          {context: projekcija.sala},
          {context: pocetak}
        ]
      });
      
    });
  }

  ngOnInit(): void {
    this.httpClient
      .get<Film>(
        "http://localhost:3000/film/"+this.aRoute.snapshot.params.id
      )
      .subscribe(
        film => {
          this.film = film;
        },
        err => {

        }
      );
    
    this.findAllProjekcije();

    this.table = new Table({
      header: {
        headerItems: [
          {context: "id"},
          {context: "id fila"},
          {context: "sala"},
          {context: "pocetak"}
        ]
      }
    });

    console.log(this.aRoute.snapshot.params);
  }

}
