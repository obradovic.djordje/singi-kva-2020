/*
    observers nisu nista drugo do same funkcije koja treba da se pozove kada nad
    objektom tipa Observable na koju smo subscribe-ovani dodje do promena

    primer observer-a:

export class AppComponent implements OnInit, OnDestroy {

    articleObserver = (observable: ArticleRepository): void => {
        console.log(observable);
        this.articles = observable.getArticles();
    }

    // interfaces
    ngOnInit() {
        this.articleRepository.attach(this.articleObserver);

        this.articles = this.articleRepository.getArticles();
        console.log("AppComponent init");
    }

    ngOnDestroy() {
        if (this.articleRepository)
            this.articleRepository.dettach(this.articleObserver);
        console.log("AppComponent destroyed");
    }
}
*/

export abstract class Observable {
    protected observers: any[] = [];

    attach = (callback: Function): void => {
        this.observers.push(callback);
    }

    dettach = (callback: Function): void => {
        this.observers = this.observers.filter(o => o !== callback);
    }

    notify = (): void => {
        this.observers.forEach(observer => {
            observer(this);
        });
    }
}