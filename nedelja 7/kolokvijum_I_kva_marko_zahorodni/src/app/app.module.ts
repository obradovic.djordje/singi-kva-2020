import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { SharedModule } from './shared/shared.module';
import { CoreModule } from './core.module';
import { MyAngularTableModule } from 'my-angular-table';
import { MatNativeDateModule } from '@angular/material/core';


// components
import { AppComponent } from './app.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { HttpClientModule } from '@angular/common/http';
import { FilmoviComponent } from './filmovi/filmovi.component';
import { ProjekcijeComponent } from './projekcije/projekcije.component';
import { KarteComponent } from './karte/karte.component';
import { AddFilmComponent } from './filmovi/add-film/add-film.component';
import { AddProjekcijaComponent } from './projekcije/add-projekcija/add-projekcija.component';
import { AddKartaComponent } from './karte/add-karta/add-karta.component';
import { UpdateFilmComponent } from './filmovi/update-film/update-film.component';
import { UpdateProjekcijaComponent } from './projekcije/update-projekcija/update-projekcija.component';
import { UpdateKartaComponent } from './karte/update-karta/update-karta.component';
import { DetailsFilmComponent } from './filmovi/details-film/details-film.component';

@NgModule({
  declarations: [
    AppComponent,
    WelcomeComponent,
    NotFoundComponent,
    FilmoviComponent,
    ProjekcijeComponent,
    KarteComponent,
    AddFilmComponent,
    AddProjekcijaComponent,
    AddKartaComponent,
    UpdateFilmComponent,
    UpdateProjekcijaComponent,
    UpdateKartaComponent,
    DetailsFilmComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,

    MatNativeDateModule,

    AppRoutingModule,
    MyAngularTableModule,
    HttpClientModule,

    SharedModule,
    CoreModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
