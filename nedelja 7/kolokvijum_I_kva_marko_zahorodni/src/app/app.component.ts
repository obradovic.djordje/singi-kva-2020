import { Component } from '@angular/core';
import { MainService } from './shared/service/main.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'kolokvijum';

  constructor(private mainService: MainService){}
}
