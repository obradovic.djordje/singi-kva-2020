import { NgModule } from "@angular/core";
import { Routes, RouterModule } from '@angular/router';

// components
import { WelcomeComponent } from './welcome/welcome.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { FilmoviComponent } from './filmovi/filmovi.component';
import { ProjekcijeComponent } from './projekcije/projekcije.component';
import { KarteComponent } from './karte/karte.component';
import { AddFilmComponent } from './filmovi/add-film/add-film.component';
import { AddProjekcijaComponent } from './projekcije/add-projekcija/add-projekcija.component';
import { AddKartaComponent } from './karte/add-karta/add-karta.component';
import { UpdateFilmComponent } from './filmovi/update-film/update-film.component';
import { UpdateProjekcijaComponent } from './projekcije/update-projekcija/update-projekcija.component';
import { UpdateKartaComponent } from './karte/update-karta/update-karta.component';
import { DetailsFilmComponent } from './filmovi/details-film/details-film.component';

const routes: Routes = [
    { path: '', component: WelcomeComponent },
    { path: 'filmovi', component: FilmoviComponent },
    { path: 'filmovi/add', component: AddFilmComponent },
    { path: 'filmovi/update/:id', component: UpdateFilmComponent },
    { path: 'filmovi/details/:id', component: DetailsFilmComponent },
    { path: 'projekcije', component: ProjekcijeComponent },
    { path: 'projekcije/add', component: AddProjekcijaComponent },
    { path: 'projekcije/update/:id', component: UpdateProjekcijaComponent },
    { path: 'karte', component: KarteComponent },
    { path: 'karte/add', component: AddKartaComponent },
    { path: 'karte/update/:id', component: UpdateKartaComponent },
    { path: '**', component: NotFoundComponent }
]

@NgModule({
    imports: [
        RouterModule.forRoot(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule {}