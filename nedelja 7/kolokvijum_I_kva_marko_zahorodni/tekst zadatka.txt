Kolokvijum 1
Napraviti Angular aplikaciju po sledećim zahtevima:

Podići json-server(https://github.com/typicode/json-server) incijalizovan podacima datim u listingu. Za svaku od dobijenih ruta obezbediti po jednu komponentu za tabelarni prikaz svih podataka na ruti. Iz tabelarnog prikaza omogućiti prikaz detalja izabranog entiteta i njegovo brisanje
Napraviti komponente za prikaz detalja pojedinačnog entiteta, kao i komponente za dodavanje entiteta. Pri kreiranju entiteta koji sadrže veze ka drugim entitetima obezbediti da se izbor povezanih entiteta vrši kroz padajuće liste.
Podesiti rutiranje tako da se za svaki entitet omogući pristup prikazu tabele i forme za dodavanje na jednoj ruti, na drugoj ruti prikaz podataka pojedinačnog entiteta i na trećoj ruti prikaz forme za izmenu podataka entiteta.
U komponenti za pojedinačni prikaz filma dodati tabelarni prikaz svih predstojećih projekcija filma.
Napraviti komponentu za prikaz ocene kao progress bar-a. Ocena je u rasponu od 0 do 10.

{
   "film": [
      {
         "id": 1,
         "naziv": "1917",
         "reziser": "Sam Mendes",
         "ocena": 7.5
      },
      {
         "id": 2,
         "naziv": "Blade Runner",
         "reziser": "Ridley Scott",
         "ocena": 8.0
      },
      {
         "id": 3,
         "naziv": "Aliens",
         "reziser": "James Cameron",
         "ocena": 8.3
      },
      {
         "id": 4,
         "naziv": "Alien",
         "reziser": "Ridley Scott",
         "ocena": 8.4
      }
   ],
   "projekcija": [
      {
         "id": 1,
         "filmId": 1,
         "sala": "S1",
         "pocetak": "2020-01-10 23:00"
      },
      {
         "id": 2,
         "filmId": 1,
         "sala": "S2",
         "pocetak": "2020-05-18 15:00"
      },
      {
         "id": 3,
         "filmId": 4,
         "sala": "S4D",
         "pocetak": "2020-04-14 17:30"
      },
      {
         "id": 4,
         "filmId": 4,
         "sala": "S3D",
         "pocetak": "2020-02-20 18:00"
      }
   ],
   "karta": [
      {
         "id": 1,
         "projekcijaId": 3,
         "cena": 400.0
      },
      {
         "id": 2,
         "projekcijaId": 1,
         "cena": 320.0
      },
      {
         "id": 3,
         "projekcijaId": 2,
         "cena": 240.0
      },
      {
         "id": 4,
         "projekcijaId": 1,
         "cena": 600.0
      }
   ]
}