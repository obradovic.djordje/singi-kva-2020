import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';

class LoginRes{
  token:string;
}

class StudentRes{
  lista:string;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'auth-example';
  username = '';
  password = '';
  authtoken = null;

  constructor(private http: HttpClient) { }

  login(){
    let user = {username: this.username, password: this.password}
    this.http.put<LoginRes>('http://localhost:8081/api/login/', user).subscribe(res=>{
      console.log(res.token);
      this.authtoken = res.token;
    })
  }

  lista(){
    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'auth-token': this.authtoken
      })
    };  

    this.http.get<StudentRes>('http://localhost:8081/api/students/', httpOptions)
    .subscribe(res=>{
      console.log(res);
    }, err=>console.log('error', err))

  }


}
