import { Component, OnInit } from '@angular/core';

import Map from 'ol/Map.js';
import View from 'ol/View.js';
import Draw from 'ol/interaction/Draw.js';
import OlZoom from 'ol/control/Zoom.js';
import Polygon from 'ol/geom/Polygon.js';
import {Tile as TileLayer, Vector as VectorLayer} from 'ol/layer.js';
import {OSM, Vector as VectorSource} from 'ol/source.js';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'
  ]
})
export class AppComponent {
  title = 'openlayer';
  raster = undefined;
  source = undefined;
  vector = undefined;
  map = undefined;
  draw = undefined;
  drawingFeatures = [];


  ngOnInit() {
    this.raster = new TileLayer({
      source: new OSM()
    });

    this.source = new VectorSource({wrapX: false});

    this.vector = new VectorLayer({
      source: this.source
    });

    this.map = new Map({
      layers: [this.raster, this.vector],
      target: 'map',
      view: new View({
        center: [-11000000, 4600000],
        zoom: 4
      }),
      controls: [
        new OlZoom({ className: 'custom-zoom' })
      ]      
    });


    this.draw = new Draw({
      source: this.source,
      type: 'LineString',
      features: this.drawingFeatures
    });
    this.map.addInteraction(this.draw);

  }

  debug(){
    console.log('-- debug --');
    for(let el in this.drawingFeatures){
      console.log(this.drawingFeatures[el]);
    }
  }


}
