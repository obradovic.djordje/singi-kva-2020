import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RacunComponent } from './racun/racun.component';
import { RacuniComponent } from './racuni/racuni.component';


const routes: Routes = [
  {path: "racuni", component: RacuniComponent},
  {path: "racuni/:brojRacuna", component: RacunComponent},
  {path: "**", component: RacuniComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
