import { Component, OnInit } from '@angular/core';
import { RacunService } from '../service/racun.service';
import { Racun } from '../model/racun';
import { Router } from '@angular/router';

@Component({
  selector: 'app-racuni',
  templateUrl: './racuni.component.html',
  styleUrls: ['./racuni.component.css']
})
export class RacuniComponent implements OnInit {
  racuni: Racun[];

  constructor(private rs: RacunService, private router: Router) { }

  ngOnInit(): void {
    this.dobaviSve();
  }

  dobaviSve() {
    this.rs.getRacuni().subscribe(racuni => this.racuni = racuni);
  }

  prikazi(brojRacuna) {
    this.router.navigate(["/racuni", brojRacuna])
  }

  ukloni(brojRacuna) {
    this.rs.deleteRacun(brojRacuna).subscribe(() => this.dobaviSve());
  }
}
