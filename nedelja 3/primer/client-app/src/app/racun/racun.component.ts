import { Component, OnInit } from '@angular/core';
import { RacunService } from '../service/racun.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Racun } from '../model/racun';

@Component({
  selector: 'app-racun',
  templateUrl: './racun.component.html',
  styleUrls: ['./racun.component.css']
})
export class RacunComponent implements OnInit {
  racun: Racun = {brojRacuna: "", stanje: null};

  constructor(private rs: RacunService, private activatedRoute: ActivatedRoute, private router : Router) { }

  ngOnInit(): void {
    this.rs.getRacun(this.activatedRoute.snapshot.params["brojRacuna"]).subscribe(r => this.racun = r);
  }

  submit() {
    this.rs.updateRacun(this.activatedRoute.snapshot.params["brojRacuna"], this.racun).subscribe(r => this.router.navigate(['/racuni']));
  }
}
