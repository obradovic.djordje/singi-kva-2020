import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Racun } from '../model/racun';

@Injectable({
  providedIn: 'root'
})
export class RacunService {

  constructor(private http: HttpClient) {}

  getRacuni(): Observable<Racun[]> {
    return this.http.get<Racun[]>("http://localhost:8080/api/racuni");
  }

  getRacun(brojRacuna): Observable<Racun> {
    return this.http.get<Racun>(`http://localhost:8080/api/racuni/${brojRacuna}`);
  }

  addRacun(racun: Racun): Observable<Racun> {
    return this.http.post<Racun>("http://localhost:8080/api/racuni", racun);
  }
  
  updateRacun(brojRacuna, racun): Observable<Racun> {
    return this.http.put<Racun>(`http://localhost:8080/api/racuni/${brojRacuna}`, racun);
  }

  deleteRacun(brojRacuna): Observable<Racun> {
    return this.http.delete<Racun>(`http://localhost:8080/api/racuni/${brojRacuna}`);
  }
}
