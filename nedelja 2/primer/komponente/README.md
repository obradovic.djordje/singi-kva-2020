# Komponente

Primer razdavajanja Angular aplikacije na komponente. Cilj je napraviti komponente za komentare i od njih sastaviti aplikaciju koju korisnici mogu koristiti za razmenu komentara. Aplikacija je sačinjena od sledećih komponenti:

* komentari - Komponenta za prikaz i upravljanje kolekcijom komentara.
* komentar - Komponenta za prikaz i upravljanje jednim komentarom.
* komentar-forma - Komponenta koja predstavlja formu za dodavanje komentara.

Pored razdvajanja na komponente cilj projekta je i da demonstrira razmenu podataka izmedju komponenti upotrebom @Input i @Output dekoratora. Detalji vezani za ove dekoratore mogu se pronaći u zvaničnoj dokumentaciji na stranici [Template syntax - @Input() and @Output() properties](https://angular.io/guide/template-syntax#input-and-output-properties).

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.0.5.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
