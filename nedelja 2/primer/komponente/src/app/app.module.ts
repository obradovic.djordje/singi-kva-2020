import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms'

import { AppComponent } from './app.component';
import { KomentarComponent } from './komentar/komentar.component';
import { KomentariComponent } from './komentari/komentari.component';
import { KomentarFormaComponent } from './komentar-forma/komentar-forma.component';

@NgModule({
  declarations: [
    AppComponent,
    KomentarComponent,
    KomentariComponent,
    KomentarFormaComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
