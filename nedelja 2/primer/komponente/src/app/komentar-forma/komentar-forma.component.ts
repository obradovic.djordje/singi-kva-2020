import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Komentar } from 'src/model/komentar';

@Component({
  selector: 'app-komentar-forma',
  templateUrl: './komentar-forma.component.html',
  styleUrls: ['./komentar-forma.component.css']
})
export class KomentarFormaComponent implements OnInit {
  // Atribut koji se vezuje za input polje za popunjavanje naziva autora.
  autor: string;
  // Atribut koji se vezuje za input polje za popunjavanje sadrzaja komentara.
  sadrzaj: string;

  // Ovim atributom se omogucava razmena podataka od ove komponente ka roditeljskoj
  // komponenti. Razmena podataka se implementira preko dogadjaja. Roditeljska komponenta
  // se pretplacuje na navedeni dogadjaj. Prilikom desavanja dogadjaja roditeljska komponenta
  // moze reagovati i azurirati se spram dogadjaja. U ovom slucaju dogadjaj ce nositi podatke
  // o komentaru. Izazivanje samog dogajaja je nadleznost komponente u kojoj je dogadjaj definisan.
  @Output()
  onSubmit:EventEmitter<Komentar> = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  preuzmiKomentar() {
    // Obavestavanje pretplatnika, roditeljske komponente, da se desio dogadjaj predavanja sadrzaja forme.
    // Odnosno prosledjivanje novog komentara roditeljskoj komponenti.
    this.onSubmit.emit({
      'autor': this.autor,
      'sadrzaj': this.sadrzaj,
      'datumObjave': new Date(),
      'odgovori': []
    });
    // Praznjenje forme nakon dodavanja komentara.
    this.autor = '';
    this.sadrzaj = '';
  }
}
