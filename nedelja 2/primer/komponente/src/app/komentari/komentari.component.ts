import { Component, OnInit, Input } from '@angular/core';
import { Komentar } from 'src/model/komentar';

@Component({
  selector: 'app-komentari',
  templateUrl: './komentari.component.html',
  styleUrls: ['./komentari.component.css']
})
export class KomentariComponent implements OnInit {
  // Kolekcija komentara koja se vezuje za komponentu za prikaz i dodavanje komentara.
  // Neko od spolja, roditeljska komponenta, ce obezbediti podatke kojima se puni ovaj atribut.
  @Input()
  komentari: Komentar[];
  // Boolean promenljiva koja odredjuje da li prikaz komentara treba uvuci ili ne.
  // Komentari se uvlace kako bi se naglasilo da predstavljaju odgovore na komentar
  // ispod kojeg se nalaze.
  @Input()
  uvlacenje:boolean = false;
  // Boolean promenljiva koja odredjuje da li ce forma za dodavanje biti prikazana ili ne.
  @Input()
  forma:boolean = true;

  constructor() { }

  ngOnInit(): void {
  }

  //  Metoda za dodavanje novog komentara.
  dodajKomentar(komentar: Komentar) {
    this.komentari.push(komentar);
  }
}
