import { Component, OnInit, Input } from '@angular/core';
import { Komentar } from 'src/model/komentar';

@Component({
  selector: 'app-komentar',
  templateUrl: './komentar.component.html',
  styleUrls: ['./komentar.component.css']
})
export class KomentarComponent implements OnInit {
  // Objekat tipa komentar koji je vezan za ovu komponentu.
  @Input()
  komentar: Komentar;

  // Boolean vrednost koja odredjuje da li ce forma za odgovor biti prikazana ili ne.
  prikazForme:boolean = false;

  constructor() { }

  ngOnInit(): void {
  }

  // Metoda za dodavanje odgovora.
  dodajOdgovor(odgovor: Komentar) {
    this.komentar.odgovori.push(odgovor);
    this.prikazForme = false;
  }

  // Metoda za promenu vidljivosti forme za odgovor.
  formaToggle() {
    this.prikazForme = !this.prikazForme;
  }
}
