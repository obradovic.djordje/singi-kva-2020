import { Component } from '@angular/core';
import { Komentar } from 'src/model/komentar';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  // Kolekcija svih komentara.
  komentari: Komentar[] = [
    {
      'autor': 'Pera Perić',
      'sadrzaj': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc dapibus nunc sit amet iaculis maximus. Integer congue gravida ex, nec tempor velit suscipit sed. Cras quis faucibus urna. Integer nec lectus non enim eleifend sagittis. Interdum et malesuada fames ac ante ipsum primis in faucibus. Etiam a vulputate neque, in tempor lectus. Pellentesque id nisl at enim tincidunt rutrum non laoreet libero. Fusce eu tristique nunc, et hendrerit libero. Nullam accumsan accumsan vehicula. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Fusce in ex in lacus aliquam finibus vehicula et mi. Pellentesque et condimentum diam. Donec sed ullamcorper quam. Nulla imperdiet finibus nulla id congue.',
      'datumObjave': new Date('2020-01-10 20:00'),
      'odgovori': [{
        'autor': 'Marko Marković',
        'datumObjave': new Date('2020-01-10 21:15'),
        'sadrzaj': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc dapibus nunc sit amet iaculis maximus. Integer congue gravida ex, nec tempor velit suscipit sed. Cras quis faucibus urna. Integer nec lectus non enim eleifend sagittis. Interdum et malesuada fames ac ante ipsum primis in faucibus. Etiam a vulputate neque, in tempor lectus. Pellentesque id nisl at enim tincidunt rutrum non laoreet libero. Fusce eu tristique nunc, et hendrerit libero. Nullam accumsan accumsan vehicula. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Fusce in ex in lacus aliquam finibus vehicula et mi. Pellentesque et condimentum diam. Donec sed ullamcorper quam. Nulla imperdiet finibus nulla id congue.',
        'odgovori': []
      }
      ]
    }, {
      autor: 'Jovan Jovanović',
      datumObjave: new Date('2020-02-11 13:32'),
      sadrzaj: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc dapibus nunc sit amet iaculis maximus. Integer congue gravida ex, nec tempor velit suscipit sed. Cras quis faucibus urna. Integer nec lectus non enim eleifend sagittis. Interdum et malesuada fames ac ante ipsum primis in faucibus. Etiam a vulputate neque, in tempor lectus. Pellentesque id nisl at enim tincidunt rutrum non laoreet libero. Fusce eu tristique nunc, et hendrerit libero. Nullam accumsan accumsan vehicula. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Fusce in ex in lacus aliquam finibus vehicula et mi. Pellentesque et condimentum diam. Donec sed ullamcorper quam. Nulla imperdiet finibus nulla id congue.',
      odgovori: []
    }
  ]
}
