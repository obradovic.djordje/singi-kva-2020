// Interfejs koji opisuje jedan komentar.
export interface Komentar {
    autor: string;
    sadrzaj: string;
    datumObjave: Date;
    odgovori: Komentar[];
}