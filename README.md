# singi-kva-2020

Raspored tema po nedeljama nastave:

## 1) 3.3.2020.utorak
  - Angular 6. podesavanje okruzenja
  - instalacija Angular CLI
  - kreiranje jednostavnog projekta sa jednom jednostavnom komponentom
  - serviranje aplikacije
  - Student ce umeti da napravi jednostavan primer Angular 6 aplikacije

## 2) 10.3.2020. utorak
  - Typescript 
  - Zasto uopste TS?
  - Prevodjenje u JS? 
  - Dosta primera u kojima se demonstriraju svi elementi TS

## 3) 17.3.2020. utorak
   - Arhitektura Angular aplikacije
   - Komponente i sabloni
   - Razmena poruka publisher - subscriber
   - Bootstraping
   - NgModules
   - Dependency Injection

## 4) 24.3.2020. utorak
   - HttpClient
   - Routing & navigation
   - Animations
## 5) 31.3.2020. utorak
   - Slozeni primeri u kojem se demonstriraju funkcije:
      - forma, lista, pretrazivanje, tree-view
## 6) 7.4.2020. utorak
    - Kolokvijum
    - ocena 6: kreirati jednostavnu angular 6 aplikaciju sa jednom listom, dodavanjem elementata u listu, obrada i pretrazivanje
    - ocena 8: prosiriti prethodni primer sa slozenijom navigacijom
    - ocena 10: prosiriti prethodne primere sa koriscenjem asinhronih HTTP poziva

## 7) 14.4.2020. utorak
    - Koriscenje eksternih modula/javascript biblioteka 
    - D3.js

## 8) 21.4.2020. utorak
    - Koriscenje eksternih modula
    - Interaktivni kalendar i povezivanje sa google kalendarom

## 9) 28.4.2020. utorak
    - Koriscenje eksternih modula
    - FileUpload
    - WEBSOCKET
    - Chat

## 10) 5.5.2020. utorak
    - Koriscenje eksternih modula
    - openlayers/mapa

## 11) 12.5.2020. utorak
    - WebGL
    - Three.js

## 12) 19.5.2020. utorak
    - Kolokvijum II
    - ocena 6: kreirati angular aplikaciju sa prikazom nekoliko vrsta grafikona
    - ocena 8: interaktivno kreiranje rasporeda predavanja
    - ocena 10: prethodno sa mogucnoscu interakcije vise korisnika

## 13) 26.5.2020. utorak
    - Testiranje Protractor
    - Deployment: WebPACK
## 14) 2.6.2020. utorak
    - ReactJS
    - VueJS
## 15) 9.6.2020. utorak
    Zavrsni projekti/ priprema
