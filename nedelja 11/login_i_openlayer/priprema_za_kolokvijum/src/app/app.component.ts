import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';

import Map from 'ol/Map.js';
import View from 'ol/View.js';
import Draw from 'ol/interaction/Draw.js';
import OlZoom from 'ol/control/Zoom.js';
import Polygon from 'ol/geom/Polygon.js';
import {Tile as TileLayer, Vector as VectorLayer} from 'ol/layer.js';
import {OSM, Vector as VectorSource} from 'ol/source.js';
import {fromLonLat} from 'ol/proj';


class LoginRes{
  token:string;
}

class MestaRes{
  lista:[];
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'kolokvijum';
  username = '';
  password = '';
  mesta = [];
  authtoken = null;
  status = '';
  payload = '';

  raster = undefined;
  source = undefined;
  vector = undefined;
  map = undefined;
  draw = undefined;
  drawingFeatures = [];

  constructor(private http: HttpClient) { }

  ngOnInit() {
    this.raster = new TileLayer({
      source: new OSM()
    });

    this.source = new VectorSource({wrapX: false});

    this.vector = new VectorLayer({
      source: this.source
    });

    this.map = new Map({
      layers: [this.raster, this.vector],
      target: 'map',
      view: new View({
        center: fromLonLat([19.833437, 45.263172]),
        zoom: 7
      }),
      controls: [
        new OlZoom({ className: 'custom-zoom' })
      ]      
    });


    this.draw = new Draw({
      source: this.source,
      type: 'LineString',
      features: this.drawingFeatures
    });
    this.map.addInteraction(this.draw);

  }

  login(){
    let user = {username: this.username, password: this.password}
    this.http.put<LoginRes>('http://localhost:8081/api/login/', user).subscribe(res=>{
      console.log(res.token);
      this.authtoken = res.token;
      this.payload = atob(this.authtoken.split('.')[1])
      this.status = 'ok'
    }, err=>this.status = 'error')
  }

  lista(){
    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'auth-token': this.authtoken
      })
    };  

    this.http.get<MestaRes>('http://localhost:8081/api/mesta/', httpOptions)
    .subscribe(res=>{
      console.log(res);
      this.mesta = res.lista;
    }, err=>console.log('error', err))

  }


}
