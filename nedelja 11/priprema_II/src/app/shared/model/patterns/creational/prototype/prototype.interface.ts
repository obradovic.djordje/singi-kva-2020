export declare interface Prototype<T> {
    clone(): T;
    prototype(t: T): void;
}