import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from '../model/patterns/behavioural/observer/observable.model';

import { Film } from 'src/app/filmovi/filmovi.component';
import { Projekcija } from 'src/app/projekcije/projekcije.component';
import { Karta } from 'src/app/karte/karte.component';

@Injectable()
export class MainService extends Observable {
    public filmovi: Film[] = [];
    public projekcije: Projekcija[] = [];
    public karte: Karta[] = [];   
    
    constructor(private httpClient: HttpClient) {
        super();
        this.findAllFilm();
        this.findAllKarte();
        this.findAllProjekcije();
    }

    findAllFilm = (): void => {
    this.httpClient
        .get(
            "http://localhost:3000/film"
        )
        .subscribe(
            (filmovi: Film[]) => {
                filmovi.forEach((film: Film) => {
                    // console.log(film);
                });
                this.filmovi = filmovi;
                // console.log(this.filmovi);
                this.notify();
            },
            err => {
                console.log(err);
            }
        );
    }

    findAllKarte = (): void => {
        this.httpClient
            .get(
                "http://localhost:3000/karta"
            )
            .subscribe(
                (karte: Karta[]) => {
                    karte.forEach((karta: Karta) => {
                        // console.log(karta);
                    });

                    this.karte = karte;
                    this.notify();
                },
                err => {
                    console.log(err);
                }
            );
    }

    findAllProjekcije = (): void => {
        this.httpClient
            .get(
                "http://localhost:3000/projekcija"
            )
            .subscribe(
                (projekcije: Projekcija[]) => {
                projekcije.forEach((projekcija: Projekcija) => {
                    //   console.log(projekcija);
                    });

                    this.projekcije = projekcije;
                    this.notify();
                },
                err => {
                    console.log(err);
                }
            );
        }

    notify = (): void => {
        this.observers.forEach(observer => {
            observer(this);
        });
    }
}