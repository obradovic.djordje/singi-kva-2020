import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MainService } from 'src/app/shared/service/main.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Projekcija } from 'src/app/projekcije/projekcije.component';
import { NgForm } from '@angular/forms';
import { Karta } from '../karte.component';

@Component({
  selector: 'app-update-karta',
  templateUrl: './update-karta.component.html',
  styleUrls: ['./update-karta.component.css']
})
export class UpdateKartaComponent implements OnInit, OnDestroy {
  public projekcije: Projekcija[] = [];
  public karta: Karta = {
    id: 0,
    cena: 0,
    projekcijaId: 0
  }

  constructor(private httpClient: HttpClient, private mainService: MainService, private aRoute: ActivatedRoute, private router: Router) { }

  onSubmit = (form: NgForm): void => {
    // console.log(form);
    if(form.valid) {

      // console.log(this.karta);

      this.httpClient
        .put(
          "http://localhost:3000/karta/"+this.aRoute.snapshot.params.id,
          this.karta
        )
        .subscribe(
          result => {
            this.mainService.findAllKarte();
            this.router.navigate(['/karte']);
          },
          err => {

          }
        )
    }
  }

  findAllProjekcije = (): void => {
    this.httpClient
        .get(
            "http://localhost:3000/projekcija"
        )
        .subscribe(
            (projekcije: Projekcija[]) => {
            projekcije.forEach((projekcija: Projekcija) => {
                //   console.log(projekcija);
                });

                this.projekcije = projekcije;
            },
            err => {
                console.log(err);
            }
        );
    }

  ngOnInit(): void {
    this.httpClient
      .get<Karta>(
        "http://localhost:3000/karta/"+this.aRoute.snapshot.params.id
      )
      .subscribe(
        karta => {
          this.karta = karta;
        },
        err => {

        }
      );

    this.findAllProjekcije();
  }

  ngOnDestroy(): void {

  }

}
