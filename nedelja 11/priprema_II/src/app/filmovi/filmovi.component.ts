import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Table, Row } from 'my-angular-table';
import { MainService } from '../shared/service/main.service';
import { Router } from '@angular/router';

// interface
export declare interface Film {
    id: number,
    naziv: string,
    reziser: string,
    ocena: number
  }

@Component({
  selector: 'app-filmovi',
  templateUrl: './filmovi.component.html',
  styleUrls: ['./filmovi.component.css']
})
export class FilmoviComponent implements OnInit, OnDestroy {
  public table: Table;
  constructor(private httpClient: HttpClient, private mainService: MainService, private router: Router) {}

  findAll = (): void => {
    this.mainService
    this.httpClient
      .get(
          "http://localhost:3000/film"
      )
      .subscribe(
          (filmovi: Film[]) => {
              filmovi.forEach((film: Film) => {
                  // console.log(film);
              });

              this.mainService.filmovi = filmovi;
              this.updateTable();
          },
          err => {
              console.log(err);
          }
      );
  }

  onDeleteFilm = (filmovi: Film[]): void => {
    // console.log(filmovi);
    filmovi.forEach((film: Film) => {
      // console.log(film);
      this.httpClient
        .delete(
          "http://localhost:3000/film/"+film.id
        )
        .subscribe(
          result => {
            console.log(result);
            this.findAll();
          },
          err => {
            console.log(err);
          }
        )
    });
  }

  updateTable = (): void => {
    // deleting
    this.table.getChildren().forEach((r: Row) => this.table.removeChild(r));
    
    this.mainService.filmovi.forEach((film: Film) => {
      // console.log(film);

      this.table.addChildValue({
        data: film,
        rowItems: [
          {context: film.id},
          {context: film.naziv},
          {context: film.reziser},
          {context: film.ocena},
          {context: "delete", button: {function: this.onDeleteFilm}, imgSrc: "https://img.icons8.com/dusk/2x/filled-trash.png"},
          {context: "update", button: {function: this.onUpdateFilm}, imgSrc: "https://img.icons8.com/nolan/2x/approve-and-update.png"},
          {context: "details", button: {function: this.onDetailFilm}, imgSrc: "https://img.icons8.com/nolan/2x/approve-and-update.png"}
        ]
      });
      
    });
  }

  onDetailFilm = (filmovi: Film[]): void => {
    this.router.navigate(['filmovi', 'details', filmovi[0].id]);
  }

  onUpdateFilm = (filmovi: Film[]): void => {
    this.router.navigate(['filmovi', 'update', filmovi[0].id]);
  }

  ngOnInit(): void {
    this.mainService.attach(this.updateTable);

    this.table = new Table({
      header: {
        headerItems: [
          {context: "id"},
          {context: "naziv"},
          {context: "reziser"},
          {context: "ocena"},
          {context: ""},
          {context: ""},
          {context: ""}
        ]
      },
      contextMenu: {
        contextMenuItems: [
          {context: "delete", function: this.onDeleteFilm, imgSrc: "https://img.icons8.com/dusk/2x/filled-trash.png"}
        ]
      }
    });

    this.updateTable();

    console.log("FilmoviComponent init");
  }

  ngOnDestroy(): void {
    if (this.mainService)
      this.mainService.dettach(this.updateTable);
    console.log("FilmoviComponent destroyed");
  }

}
