import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { MainService } from 'src/app/shared/service/main.service';
import { Film } from '../filmovi.component';
import { Projekcija } from 'src/app/projekcije/projekcije.component';

@Component({
  selector: 'app-update-film',
  templateUrl: './update-film.component.html',
  styleUrls: ['./update-film.component.css']
})
export class UpdateFilmComponent implements OnInit {
  public film: Film = {
    id: 0,
    naziv: "",
    ocena: 0,
    reziser: ""
  };

  constructor(private httpClient: HttpClient, private mainService: MainService, private aRoute: ActivatedRoute, private router: Router) { }

  onSubmit = (form: NgForm): void => {
    console.log(form);
    if(form.valid) {

      // let film = {
      //   naziv: form.value.naziv,
      //   reziser: form.value.reziser,
      //   ocena: form.value.ocena
      // }

      console.log(this.film);

      this.httpClient
        .put(
          "http://localhost:3000/film/"+this.aRoute.snapshot.params.id,
          this.film
        )
        .subscribe(
          result => {
            this.mainService.findAllFilm();
            this.router.navigate(['/filmovi']);
          },
          err => {

          }
        )
    }
  }

  ngOnInit(): void {

    this.httpClient
      .get<Film>(
        "http://localhost:3000/film/"+this.aRoute.snapshot.params.id
      )
      .subscribe(
        film => {
          this.film = film;
        },
        err => {

        }
      );

    console.log(this.aRoute.snapshot.params);
  }

}
