import { Component } from '@angular/core';


class Korisnik{
  username: string;
  ime: string;
  prezime: string;
  adresa: string;

  constructor(username: string){
    this.username = username;
    this.ime = '';
    this.prezime = '';
    this.adresa = '';
  }
}


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'tacka1';

  korisnici = [ new Korisnik('pera'), new Korisnik('mika')]
  selektovaniKorisnik = null;

  addNew(){

  }

  select(korisnik: Korisnik){
    this.selektovaniKorisnik = korisnik;
  }

  delete(korisnik: Korisnik){
    // varijanta 1
    this.korisnici = this.korisnici.filter(obj => obj !== korisnik);
    // this.drzave = this.drzave.filter(obj=> obj !== drzava)

    // varijanta 2
    // let indeks = null;
    // for(let i in this.korisnici){
    //   if(korisnik == this.korisnici[i]){
    //     indeks = i;
    //     break;
    //   }
    // }
    // if(indeks != null){
    //   let obrisan = this.korisnici.splice(indeks, 1);
    // }
  }



}
